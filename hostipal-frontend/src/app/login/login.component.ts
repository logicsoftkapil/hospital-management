import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { NotificationsService } from 'angular2-notifications';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formData = {
    username: '',
    password: ''
  };

  constructor(private authService: AuthService, protected notificationService: NotificationsService, private router: Router) { }

  ngOnInit(): void {
    sessionStorage.clear();
  }

  reset(){
    this.formData.username = "";
    this.formData.password = "";
  }
  onSubmit() {
    if(this.formData.username == null || this.formData.username == ""){
      this.notificationService.error("",'Please enter username.');
      return;
    }
    if(this.formData.password == null || this.formData.password == ""){
      this.notificationService.error("",'Please enter password.');
      return;
    }
    this.authService.login(this.formData).subscribe(
      (response: any) => {
        this.notificationService.success("",'Login Successfully.');
        this.reset();
        this.router.navigate(['/dashboard']);
      },
      (error: any) => {
        if(error.status == 404){
          this.notificationService.alert("", 'User not found');
          this.reset();
        }
        else if(error.status == 401){
          this.notificationService.alert("", 'Invalid Password');
          this.reset();
        }
        else {
          this.notificationService.error("", 'Internal Server Error!');
        }
        console.log(error);
      }
    );
  }

}
