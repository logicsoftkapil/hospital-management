import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  apiUrl = 'http://localhost:3000/api/auth'; // Replace with your backend API URL

  constructor(private https: HttpClient) { }

  login(formData: any): Observable<any> {
    return this.https.post(`${this.apiUrl}/login`, formData);
  }
}
