import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { NotificationsService } from 'angular2-notifications';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  formData = {
    username: '',
    // email: '',
    password: ''
  };

  constructor(private userService: UserService, protected notificationService: NotificationsService, private router: Router) { }

  ngOnInit(): void {
  }

  reset(){
    this.formData.username = "";
    this.formData.password = "";
  }

  onSubmit() {
    if(this.formData.username == null || this.formData.username == ""){
      this.notificationService.error("",'Please enter username.');
      return;
    }
    if(this.formData.password == null || this.formData.password == ""){
      this.notificationService.error("",'Please enter password.');
      return;
    }
    this.userService.register(this.formData).subscribe(
      (response: any) => {
        this.notificationService.success("",'Registration Successfully.');
        this.reset();
        this.router.navigate(['/login']);
      },
      (error: any) => {
        if(error.status == 400){
          this.notificationService.error("", 'Username already exists');
          this.reset();
        }
        else{
          this.notificationService.error("", 'Internal Server Error!');
        }
        // console.log(error);
      }
    );
  }

}
