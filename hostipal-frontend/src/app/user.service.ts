import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  apiUrl = 'http://localhost:3000/api/auth'; //Replace with your backend API URL

  constructor(private http: HttpClient) { }

  register(formData: any):Observable<any> {
    return this.http.post(`${this.apiUrl}/register`, formData);
  }
}
